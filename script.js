

const itemContainer = document.querySelector('#item-container')
const items = await getAllItems()
items.forEach(function (item) {
  if(!item.position){
    // dont display items without position
    item.position = {
      x: 0.1,
      y: 0
    }
    // return
  }

  const item_state = item.state ?? 0
  const item_type = item.type

  const element = document
    .querySelector(`#template-item-${item_type == 'counter' ? 'number' : 'binary'}`)
    .content
    .cloneNode(true)
    .querySelector(item_type == 'counter' ? 'div' : 'button')

  element.dataset.itemType = item_type

  if(item.type == 'counter'){
    element
      .querySelector('span')
      .textContent = item.id

    const input = element.querySelector('input')

    input.onchange = async function (_) {
      await sendItemState(item.id, Number(input.value))
    }

    element
      .querySelector('input')
      .value = item_state
  }else{
    element.textContent = item.id

    const is_output = ['flipper', 'microcode'].includes(item.type)

    if (true || is_output) {
      element.onclick = async function (_) {
        const value = (Number(element.dataset.itemValue ?? 0) + 1) % 2
        element.style.backgroundColor = '#dddddd'
        await sendItemState(item.id, value)
      }
    }

    element.style.backgroundColor = item_state ? '#55ff55' : '#ff5555'
    element.dataset.itemValue = item_state
  }

  element.style.bottom = `${item.position.y * 100}%`
  element.style.left = `${item.position.x * 100}%`
  element.dataset.itemId = item.id

  itemContainer.appendChild(element)
})

async function sendItemState(item_id, state) {
  console.log(`sending state ${state} to item ${item_id}`)
  await call_properties_method(
    'Set',
    ['tha.flipper.ItemRegistry1', item_id, state])
}

async function getAllItems() {
  return JSON.parse(await call_registry_method('GetAllItems'))
}

async function getItemValues(){
  return await call_properties_method('GetAll')
}

function call_registry_method(method, args){
  return dbus_call_method(
    'tha.flipper.ItemRegistry1',
    'tha.flipper.ItemRegistry1',
    '/tha/flipper/ItemRegistry1',
    method,
    args
  )
}

function call_properties_method(method, args){
  return dbus_call_method(
    'tha.flipper.ItemRegistry1',
    'org.freedesktop.DBus.Properties',
    '/tha/flipper/ItemRegistry1',
    method,
    args
  )
}

async function dbus_call_method(bus_name, interface_, path, method, args=[]) {
  const url = `/api/by-interface/${interface_}/by-path${path}/methods/${method}?bus_name=${bus_name}`;

  const response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify({ 'args': args }),
    headers: {
      'Content-Type': 'application/json'
    }
  });

  if (!response.ok) {
    throw new Error(`HTTP-Fehler! Status: ${response.status}`);
  }

  const responseData = await response.json();

  if (responseData.error != undefined) {
    throw responseData.error
  }

  return responseData.response;
}


